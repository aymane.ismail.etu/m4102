package fr.ulille.iut.m4102;

import java.io.IOException;

/**
 * Cette classe introduit un intermédiaire entre la classe utilisatrice
 * et l'implémentation du traitement des chaînes.
 * Au début cette classe se contente de logger ce qui se passe puis
 * elle va évoluer pour accéder au service à distance.
 * Le comportement de cette classe est totalement transparent pour la
 * classe Utilisatrice qui au final utilise les mêmes méthodes que si elle
 * appelait directement la classe AlaChaine.
 */
public class Intermediaire implements AlaChaineInterface {
	
	private AccesService aService;
	private Client client;
	public Intermediaire() throws IOException {

		aService = new AccesService();
		//alc = new AlaChaine();
	}

	public int nombreMots(String chaine) {
		/*  System.out.println("Méthode: NombreMots ; Paramètre(String): "+chaine +" ; Resultat(int): "+alc.nombreMots(chaine));
	return alc.nombreMots(chaine); */
		
		client = new Client("localhost", 8080);

		String invocation = "Call:nombreMots:param[string," + chaine + "]";
		String resInv = aService.traiteInvocation(invocation);
		String valeur = resInv.split(":")[1].split(",")[1];
		
		String res = null;
		try {
			res = client.envoyer(resInv);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(res);

		
		return Integer.parseInt(valeur);


	}

	public String asphyxie(String chaine) throws PasDAirException {
		/* System.out.println("Méthode: asphyxie ; Paramètre(String): "+chaine +" ; Resultat(String): "+alc.asphyxie(chaine)+" ; PasDAirException: Déjà asphyxiée");
	return alc.asphyxie(chaine);*/
		
		client = new Client("localhost", 8080);
		
		String invocation = "Call:asphyxie:param[string," + chaine + "]";
		String resInv = aService.traiteInvocation(invocation);
		String valeur = resInv.split(":")[1].split(",")[1];
		
		String res = null;
		try {
			res = client.envoyer(resInv);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(res);

		
		return valeur;

	}

	public String leetSpeak(String chaine) {
		/* System.out.println("Méthode: leetSpeak ; Paramètre(String): "+chaine +" ; Resultat(String): "+alc.leetSpeak(chaine));
	return alc.leetSpeak(chaine);*/
		
		client = new Client("localhost", 8080);
		
		String invocation = "Call:leetSpeak:param[string," + chaine + "]";
		String resInv = aService.traiteInvocation(invocation);
		String valeur = resInv.split(":")[1].split(",")[1];
		
		String res = null;
		try {
			res = client.envoyer(resInv);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(res);

		
		return valeur;
	}

	public int compteChar(String chaine, char c) {
		/* System.out.println("Méthode: compteChar ; Paramètre(String, char): "+chaine+", "+c+" ; Resultat(int): "+alc.compteChar(chaine, c));
	return alc.compteChar(chaine, c);*/
		
		client = new Client("localhost", 8080);
		
		String invocation = "Call:compteChar:param[string, " + chaine + "]:param[char," + c + "]";
		String resInv = aService.traiteInvocation(invocation);
		String valeur = resInv.split(":")[1].split(",")[1];

		String res = null;
		
		try {
			res = client.envoyer(resInv);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(res);


		return Integer.parseInt(valeur);	

	}	


}
