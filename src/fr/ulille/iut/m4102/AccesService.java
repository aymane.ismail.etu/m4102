package fr.ulille.iut.m4102;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Arrays;

/**
 * Cette classe se trouvera côté service
 * Elle recevra une requête sous forme de chaîne
 * de caractère conforme à votre protocole.
 * Elle transformera cette requête en une
 * invocation de méthode sur le service puis
 * renverra le résultat sous forme de chaîne
 * de caractères.
 */
public class AccesService implements Runnable {
	private AlaChaine alc;
	private Socket s;
	
	public AccesService() throws IOException {
		alc = new AlaChaine();
	}
	public AccesService(Socket sock) {
		alc = new AlaChaine();
		s = sock;
	}


	public String traiteInvocation(String invocation) {
		System.out.println("REQUEST: " + invocation);
		String[] invocDivisee = invocation.replace("param[","").replace("]","").split(":");
		String nomMethode = invocDivisee[1];
		String[] paramTab = Arrays.copyOfRange(invocDivisee, 2, invocDivisee.length);
		
		String resultatFormate = "resultat:";

		switch(nomMethode) {
		
		case "nombreMots":
			if(paramTab[0].split(",")[0].equals("string")) {
				resultatFormate += "int," + alc.nombreMots(paramTab[0].split(",")[1]);
			}
			break;
			
		case "compteChar":
			if(paramTab[0].split(",")[0].equals("string") && paramTab[1].split(",")[0].equals("char")) {
				resultatFormate += "int," + alc.compteChar(paramTab[0].split(",")[1],paramTab[1].split(",")[1].charAt(0));	
			}
			break;
			
		case "asphyxie":
			if(paramTab[0].split(",")[0].equals("string")) {
				resultatFormate += "string,";
				try {
					resultatFormate += alc.asphyxie(paramTab[0].split(",")[1]);
				} catch (PasDAirException e) {
					resultatFormate += "error";
				}
			}
			break;
			
		case "leetSpeak":
			if(paramTab[0].split(",")[0].equals("string")) {
				resultatFormate += "string," + alc.leetSpeak(paramTab[0].split(",")[1]);
			}
			break;
		}
		
		System.out.println("RETURN: " + resultatFormate);
		return resultatFormate;

	}
	
	
	public void traiteRequete() throws IOException {
		BufferedReader reception = new BufferedReader(new InputStreamReader(s.getInputStream()));
		PrintWriter envoi =  new PrintWriter(s.getOutputStream(), true);
		String str = traiteInvocation(reception.readLine());
		System.out.println(str);
		envoi.println(str);
		reception.close();
		envoi.close();
	}
	
	public void run() {
		PrintWriter envoi = null;
		try {
			envoi = new PrintWriter(s.getOutputStream(), true);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		envoi.println("Salut client");
	}

}
