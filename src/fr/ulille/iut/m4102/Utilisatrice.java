package fr.ulille.iut.m4102;

/**
 * Cette classe va utiliser notre service.
 * Elle permettra de vérifier que notre service
 * est toujours accessible de manière transparente
 * même quand il sera sur une autre machine.
 */
public class Utilisatrice {
    
    public static void main(String[] args) throws Exception {
	// On trouve ici la seule modification par rapport à une
	// utilisation normale.
	// On instancie la classe Intermediaire au lieu
	// d'instancier directement AlaChaine.
	AlaChaineInterface i = new Intermediaire();
	
	System.out.println(i.nombreMots("Travail à la  chaîne"));
	
	try {
	    System.out.println(i.asphyxie("La moRale de l'histoire"));
	    System.out.println(i.asphyxie("j'ai soif"));
	}
	catch ( PasDAirException ex ) {
	    System.out.println("Exception " + ex.getMessage());
	}
	
	System.out.println(i.leetSpeak("l'Elite de la nation"));

	System.out.println(i.compteChar("arrête ton char", 'a'));
    }
}
